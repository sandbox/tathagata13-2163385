<?php
function wepay_getUserProfile()
{
	global $user;
	$userdata=user_load($user->uid);
	return $userdata;
}


function createForm($form)
{
	global $user;
	if($user->uid > 0)
		$user_data = wepay_getUserProfile();
	$form['personal_info'] = array(
				'#type' => 'fieldset',
				'#title' => t('Personal Informations'),								
				'#collapsible' => true,
				'#prefix' =>'<br>',
				'#suffix' =>'<br><br>',
				'#collapsed' => false,
			);	

	$form['personal_info']['username'] = array(
			'#type' => 'textfield',
			'#title' => t('User Name'),
			'#default_value' => $user_data->name,
			'#required' => TRUE,
		);
		
	$form['personal_info']['email'] = array(
			'#type' => 'textfield',
			'#title' => t('User Email'),
			'#default_value' => $user_data->mail,
			'#required' => TRUE,
		);
		
	$form['personal_info']['address'] = array(
			'#type' => 'textarea',
			'#title' => t('Address'),
			'#rows' =>'3',
			'#required' => TRUE,
		);
	$form['personal_info']['city'] = array(
			'#type' => 'textfield',
			'#title' => t('City'),
			'#required' => TRUE,
			'#size' => 20
		);
	$form['personal_info']['state'] = array(
			'#type' =>'select',
			'#options'=> array(''=>'-- select --','AK'=>'AK','AL'=>'AL','CT'=>'CT','HI'=>'HI'),
			'#title' => t('State'),
			'#required' => TRUE,
			'#attributes' => array('style'=>'width:150px')
		);
	$form['personal_info']['country'] = array(
			'#type' =>'select',
			'#options'=> array(''=>'-- select --','country1'=>'country1','country2'=>'country2'),
			'#title' => t('Country'),
			'#required' => TRUE,
			'#attributes' => array('style'=>'width:150px')
		);
	$form['personal_info']['zip'] = array(
			'#type' => 'textfield',
			'#title' => t('Zip Code'),
			'#required' => TRUE,
			'#size' => 20
		);
		
	$form['card_info'] = array(
				'#type' => 'fieldset',
				'#title' => t('Credit Card Informations'),								
				'#collapsible' => true,
				'#collapsed' => false,
			);	
		
	$form['card_info']['creditCardType'] = array(
			'#type' => 'select',
			'#title' => t('Card Type'),
			'#options'=> array(
							'Visa'=>'Visa',
							'Mastercard'=>'Mastercard',
							'Discover'=>'Discover',
							'Amex'=>'American Express'
							),
			'#required' => TRUE,							
			'#attributes' => array('style'=>'width:150px')
		);
	$form['card_info']['creditCardNumber'] = array(
			'#type' => 'textfield',
			'#title' => t('Card Number'),
			'#element_validate' => array('creditCardNumber_validate'),
			'#required' => TRUE,
		);						
	$form['card_info']['expDateMonth'] = array(
			'#type' => 'select',
			'#title' => t('Expiration Date'),
			'#options'=> array('1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07'),
			'#required' => TRUE,
			'#attributes' => array('style'=>'width:150px')
		);
	$form['card_info']['expDateYear'] = array(
			'#type' => 'select',							
			'#options'=> array('2013'=>'2013','2014'=>'2014','2015'=>'2015','2016'=>'2016','2017'=>'2017'),
			'#required' => TRUE,
			'#element_validate' => array('expDate_validate'),
			'#attributes' => array('style'=>'width:150px')
		);
	$form['card_info']['cvv2Number'] = array(
			'#type' => 'textfield',
			'#title' => t('Card Verification Number'),
			'#required' => TRUE,
			'#element_validate' => array('cvv2Number_validate'),
			'#size' => 20
		);
	$form['card_info']['amount'] = array(
			'#type' => 'textfield',
			'#title' => t('Amount'),
			'#required' => TRUE,
			'#prefix' =>'<div style="display:none">',
			'#suffix' =>'</div>',
			'#default_value' => '11',
			'#size' => 20
		);
		
	$form['#submit'][] = 'wepay_submit_handler';
					
}
/****************************************************************************************************************/
/**********************************************		VALIDATION FUNCTIONS 	*************************************/
/****************************************************************************************************************/
function creditCardNumber_validate($element)
{
  $cardnumber = preg_replace('/\D/', '', $element['#value']);

  if (!is_numeric($cardnumber)) {
    form_error($element, t('Please enter a valid credit card number.'));
  }

  $cardnumber_length = drupal_strlen($cardnumber);
  $parity = $cardnumber_length % 2;

  $total=0;
  for ($i = 0; $i < $cardnumber_length; $i++) {
    $digit = $cardnumber[$i];
    if ($i % 2 == $parity) {
      $digit *= 2;
      if ($digit > 9) {
        $digit -= 9;
      }
    }
    $total += $digit;
  }

  $valid = ($total % 10 == 0) ? TRUE : FALSE;

  if (!$valid) {
    form_error($element, t('Your card appears to be invalid. Please check the numbers and try again.'));
  }
}

function expDate_validate($element) 
{
  if ($element['#value']['year'] == date('Y', time()) && $element['#value']['month'] < date('m', time()))
  {
    form_error($element, t('Please enter a valid expiration date.'));
  }
}

function cvv2Number_validate($element)
{
  if (!is_numeric($element['#value']))
  {
    form_error($element, t('Please enter a valid CVV number.'));
  }
}
/****************************************************************************************************************/
/**********************************************		VALIDATION FUNCTIONS 	*************************************/
/****************************************************************************************************************/